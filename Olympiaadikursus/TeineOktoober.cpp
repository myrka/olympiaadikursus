#include <iostream>
#include <map>
#include <vector>

using namespace std;

void summakolmikud(int n){
	
	//Siin on brute force meetod
	//varsti saab ka rekursiivne kood kirjutatud...
	cout << "Summakolmikud arvule " << n << ":" << endl;
	for (int i = 0; i <= n; i++){
		for (int j = 0; j <= n; j++){
			for (int k = 0; k <= n; k++){
				if (i + j + k == n){
					cout << "Summakolmik: \t" << i << "\t" << j << "\t" << k << endl;
				}
			}
		}
	}
}

void primesUntil(int n){
	map<long, bool> arvud;
	for (int i = 2; i <= n; i++){
		arvud[i] = true;
	}

	for (int x = 2; x <= n; x++){
		if (arvud[x]){
			cout << "Algarv: " << x << endl;
			for (int i = x; i <= n; i+=x){
				if (arvud[i]){
					arvud[i] = false;
				}
			}
		}
	}
}

bool isPrime(long long x){
	if (x < 2)
		return false;
	long l = 2;
	while (l*l <= x){
		if (x % l++ == 0){
			return false;
		}
	}
	return true;
}

long long pow(int x, int y){
	if (y == 1){
		return x;
	}
	else if (y % 2 == 0){
		return pow(x * x, y / 2);
	}
	else{
		return x * pow(x * x, y / 2);
	}
}