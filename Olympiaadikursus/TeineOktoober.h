#ifndef TEINEOKTOOBER_H
#define TEINEOKTOOBER_H

//Tagastab summakolmikuid (�lesanne 4)
void summakolmikud(int n);

///Tagastab true, kui sisestatud arv on algarv (�lesanne 2)
bool isPrime(long long x);

///Tagastab k�ik algarvud kuni arvuni x (Eratosthenes s�el) (�lesanne 2)
void primesUntil(int x);

///Tagastab x astmel y (�lesanne 1)
long long pow(int x, int y);

#endif